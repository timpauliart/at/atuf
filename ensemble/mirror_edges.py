from moviepy.video.fx.all import crop, mirror_y
from moviepy.editor import clips_array


def mirror_edges(clip, y1, y2reverse):
    middle = crop(clip, y1=y1, y2=clip.h - y2reverse)
    top = crop(middle, y1=0, y2=y1)
    top = top.fx(mirror_y)
    bottom = crop(middle, y1=middle.h - y2reverse)
    bottom = bottom.fx(mirror_y)
    result = clips_array([[top], [middle], [bottom]])
    return result
