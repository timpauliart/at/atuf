from itertools import cycle

import numpy as np
from moviepy.editor import VideoFileClip
from moviepy.video.fx.all import crop, invert_colors, loop, mirror_x, mirror_y

from ensemble import Clipper, get_fftframefunc, mirror_edges
from utility import get_sample

from .functions import Chord, Drum, Inter


# NOTES
def splitnote(dur, clipper, x, y, width, height):
    clip = clipper.get(dur)
    clip = clip.fx(zoom, point=(x, y), width=width, height=height)
    clip = clip.fx(invert_colors)
    return clip


def part1note(pattern, clippers, videofx):
    clipper = next(clippers)
    dur = sum([p.duration for p in pattern])
    clip = clipper.get(dur)
    split_idxs = [int(x.freq * clip.w) for x in pattern]
    split_idxs = split_idxs[1:-1]
    split_idxs = 1 if not split_idxs else split_idxs
    if videofx:
        clip = clip.fl_image(get_fftframefunc(np.angle, split_idxs, 1))
    return clip


def part1internote(note, clip):
    clip = clip.subclip(0, note.delay)
    clip = clip.fx(mirror_x) if note.freq > 0.5 else clip
    clip = clip.fx(mirror_y) if note.phase > 0.5 else clip
    return clip


def part2note(note, clippers, scale, videofx):
    clipper = get_sample(note.rel_amp, clippers)
    clip = clipper.get(note.delay)
    if scale > 0 and videofx:
        clip = clip.fl_image(get_fftframefunc(np.abs, 1, scale))
    clip = clip.fx(mirror_x) if note.freq > 0.5 else clip
    clip = clip.fx(mirror_y) if note.phase > 0.5 else clip
    return clip


def part2internote(note, clip, scale, videofx):
    if scale > 0 and videofx:
        clip = clip.fl_image(get_fftframefunc(np.abs, 1, scale))
    clip = clip.fx(mirror_x) if note.freq > 0.5 else clip
    clip = clip.fx(mirror_y) if note.phase > 0.5 else clip
    return clip


def part3note(note, all_clippers, videofx):
    clippers = get_sample(note.rel_amp, all_clippers)
    clipper = next(clippers)
    clip = clipper.get(note.delay)
    if videofx:
        if note.rel_amp > 0.5:
            clip = clip.fl_image(get_fftframefunc(np.abs, 1, 1))
        else:
            clip = clip.fl_image(get_fftframefunc(np.angle, 1, 1))
    return clip


# PARTS
def make_introv(structure, splitter):
    assert(len(structure) == 1)
    iclip = VideoFileClip('video/upsized/6piano.mov')
    iclip = mirror_edges(iclip, 320, 265)
    note = structure[0]
    first = iclip.subclip(0, note.duration)
    sec_dur = note.delay - note.duration
    sec = splitnote(sec_dur, splitter, 0, 0, 1, 0)
    return [first, sec]


def make_part1v(structure, m_scenes, i_scenes, videofx):
    iiter = i_scenes.__iter__()
    robin = cycle(m_scenes)
    result = []
    for i, s in enumerate(structure):
        print(i)
        if type(s) is Chord:
            clip = part1note(s, robin, videofx)
            result.append(clip)
        elif type(s) is Inter:
            i_scene = next(iiter)
            for note in s:
                clip = part1internote(note, i_scene)
                result.append(clip)
        else:
            raise ValueError('No Chord or Inter.')

    return result


def make_splitv(structure, clipper):
    structure = list(filter(lambda x: x.delay > 0, structure))
    widths = cycle([1, 0])
    heights = cycle([0, 1])
    result = []
    for s, w, h in zip(structure, widths, heights):
        clip = splitnote(s.delay, clipper, 0, 0, w, h)
        result.append(clip)
    return result


def thebuildup():
    yield 1
    yield 2
    yield 3
    yield 4
    yield 6
    yield 8
    yield 12
    yield 16
    yield 24
    yield 32
    yield 48
    yield 64
    yield 96
    yield 128
    yield 192
    yield 256
    yield 384
    yield 512
    yield 768
    yield 1024
    yield 1536
    yield 2048
    yield 3072
    yield 4096


def make_part2v(structure, m_scenes, i_scenes, videofx):
    result = []
    factor = 2
    buildup = thebuildup()
    newscale = 1
    zoom_width = 1920
    zoom_x = 0
    for i, s in enumerate(structure):
        print(i)
        if type(s) is Drum:
            s = s * factor  # here because before drum or inter check
            oldscale = newscale
            newscale = next(buildup)
            scales = np.linspace(oldscale, newscale, len(s))
            for note, scale in zip(s, scales):
                clip = part2note(note, m_scenes, int(scale), videofx)
                result.append(clip)
        elif type(s) is Inter:
            s = s * factor
            for note in s:
                i_scene = i_scenes.get(note.delay)
                i_scenes.get(note.delay)  # double for jump
                if i > 30:  # 02:45 min
                    clip = zoom(i_scene, (zoom_x, 0), zoom_width)
                    zoom_width = int(zoom_width * 0.95)
                    zoom_x = 1920 - zoom_width
                    clip = part2internote(note, clip, oldscale, videofx)
                else:
                    clip = part2internote(note, i_scene, 0, videofx)
                result.append(clip)
        else:
            raise ValueError('No Drum or Inter.')
    return result


def make_part3v(structure, all_clippers, lastlenadd, videofx):
    structure[-1] = structure[-1].change_del(structure[-1].delay + lastlenadd)
    result = []
    for i, s in enumerate(structure):
        print(i)
        clip = part3note(s, all_clippers, videofx)
        result.append(clip)
    return result


# CLIPPER
def make_clippers():
    stove = Clipper(['video/upsized/5stove.mov'])
    mg = Clipper(['video/upsized/2mg.mov'])
    return [stove, mg]


# clippers
def make_clippers1(clippers):
    stove = clippers[0]
    coldplay = Clipper(['video/upsized/8coldplay.mov'])
    mg = clippers[1]
    return [stove, coldplay, mg]


def make_clippers2(clippers):
    stove = clippers[0]
    piano = Clipper(['video/upsized/6piano.mov'])
    mg = clippers[1]
    return [  # kick
        stove,
        # hihat
        piano,
        # snare
        mg]


def subloop(clip, start, end):
    result = clip.subclip(start, end)
    result = loop(result, duration=10)
    return result


def make_inters1():
    folder = 'video/upsized/'
    piano = VideoFileClip(folder + '6piano.mov')
    names = [(piano, 0, 20, 320, 265)]
    return cycle([subloop(mirror_edges(n, t, b), s, e) for
                  n, s, e, t, b in names])


def make_inters2():
    folder = 'video/upsized/'
    filename = '5stove.mov'
    return Clipper([folder + filename])


# UTILITY
def zoom(clip, point, width=None, height=None):
    x = int(point[0])
    y = int(point[1])
    if height is None:
        height = int(width / (clip.w / clip.h))
    if width is None:
        width = int(height / (clip.h / clip.w))
    print(x, y, width, height)
    cropped = crop(clip, x1=x, y1=y,
                   width=width, height=height)
    return cropped.resize(clip.size)
