from glob import iglob
from itertools import chain
from os.path import join

from orchestra import FractalsGenerator, fft_generate, rhythms_from, FftNote
from utility import inter_stan, square, wav_to_arrays, print_list
import numpy as np


class Drum(list):
    def __init__(self, *args):
        list.__init__(self, *args)


class Chord(list):
    def __init__(self, *args):
        list.__init__(self, *args)


class Inter(list):
    def __init__(self, *args):
        list.__init__(self, *args)


def make_gens():
    regex = join('audio', 'samples', '*.wav')
    names = list(iglob(regex, recursive=True))  # not sorted ftw
    print_list(names)
    assert(len(names) == 9)
    epsilon = 0.1
    threshold = 0.1
    sizes = [2**i for i in range(2, 10)]
    gens = []
    for s in sizes:
        cells = []
        for name in names:
            waves, sr = wav_to_arrays(name)
            tmp = rhythms_from(waves[0], square, s, epsilon, threshold)
            cells.append(tmp)
        gen = FractalsGenerator(cells)
        gens.append(gen)
    return gens


def make_interludes(gens, duration):
    macro = gens[3].generate(duration, duration / (2**4))
    interludes = []
    for m in macro:
        dur = m.duration
        index = int(round(inter_stan(m.rel_amp, 0, len(gens) - 1)))
        tmp = gens[index].generate(dur, dur)
        print('genindex:', index, 'dur:', dur, 'hits:', len(tmp))
        interludes.append(Inter(tmp))
    return interludes


def make_concrete(gens, duration):
    macro = gens[5].generate(duration, duration / (2**3))
    concrete = []
    for m, i in zip(macro, range(len(macro))):
        dur = m.duration
        index = int(round(inter_stan(m.rel_amp, 0, len(gens) - 1)))
        if i > len(macro) / 2:
            index += 4
            index = 7 if index > 7 else index
        basetime = inter_stan(m.phase, dur / 8, dur)
        tmp = gens[index].generate(dur, basetime)
        print('genindex:', index, 'dur:', dur, 'hits:', len(tmp))
        concrete.append(Drum(tmp))
    return concrete


def make_abstract(gens, duration):
    macro = gens[4].generate(duration, duration / (2**3))
    abstract = []
    for m, i in zip(macro, range(len(macro))):
        dur = m.duration
        index = int(round(inter_stan(m.rel_amp, 0, len(gens) - 1)))
        if i > len(macro) / 2:
            index += 5
            index = 7 if index > 7 else index
        tmp = gens[index].generate(dur, dur)
        print('genindex:', index, 'dur:', dur, 'hits:', len(tmp))
        abstract.append(Chord(tmp))
    return abstract


def connect(structure, interludes, length, start=0):
    assert(len(structure) > length)
    dummies = [None] * (len(structure) - length)
    result = []
    for s, i in zip(structure, interludes[start:(start + length)] + dummies):
        result.append(s)
        if i is not None:
            result.append(i)
    return result


def make_final(structure, duration):
    final = fft_generate(chain.from_iterable(structure), duration / (2**5))
    for f in final:
        print('dur', f.delay, 'amp:', f.rel_amp, 'phase:', f.phase,
              'freq:', f.freq)
    return final


def make_intro():
    return [FftNote.new(2, 1, 0, 0.25, 1, 1)]


def make_split1():
    # from mutual core
    unit = 0.125
    durs = [3, 1, 2, 3, 1]
    amp = 1
    rev = 1
    result = []
    for d in durs:
        dur = d * unit
        tmp = FftNote.new(dur, dur, 0, amp, 1, rev)
        result.append(tmp)
    last = FftNote.new(0, 0.8, 1, amp, 0.25, rev)
    result.append(last)
    return result


def make_split2(duration):
    # from crystalline
    amps = np.geomspace(0.01, 1, 32)
    pitch = 1
    rev = 1
    result = []
    for a in amps:
        tmp = FftNote.new(0.125, 0.125, 0, a, pitch, rev)
        result.append(tmp)
    delay = duration / (2**5)
    last = FftNote.new(delay, 5, 0.5, 1, pitch, rev)
    result.append(last)
    return result
